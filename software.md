# Operating Systems

- [Haiku](https://www.haiku-os.org)
- [NixOS](https://nixos.org)

# Programming Languages

- [Python](https://www.python.org)
- [Lua](https://www.lua.org)
- [MoonScript](https://moonscript.org)
- [Rust](https://www.rust-lang.org)
- [Crystal](https://crystal-lang.org)

# Social Networking

- [Pleroma](https://pleroma.social)
- [Scuttlebutt](https://www.scuttlebutt.nz)

# XMPP

- [Conversations (Android)](https://conversations.im)
- [Dino (Linux)](https://dino.im)
- [Profanity](https://profanity-im.github.io)

# Databases

- [~~Redis~~](https://redis.io)
- [KeyDB](https://github.com/JohnSully/KeyDB)
- [SQLite](https://sqlite.org/index.html)
- [PostgreSQL](https://www.postgresql.org)